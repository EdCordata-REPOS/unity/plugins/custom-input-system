# Custom Input System for Unity

Custom input system, by EdCordata
<br/>
[gitlab.com/EdCordata-REPOS/unity/plugins/custom-input-system](https://gitlab.com/EdCordata-REPOS/unity/plugins/custom-input-system)


<br/>


## Description
Custom input system, built as alternative to Unity's new input system,
to avoid limitations and simplify input process from code.


<br/>


## Installation
1) Add file [src/CustomInputSystem.cs](src/CustomInputSystem.cs) to your project.
2) Create a `GameObject` (and name it something like `Global`) and attach this script.


<br/>


## Usage
```c#
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {

    void Start() {

        // Add Shortcut
        CustomInputSystem.Instance.RegisterAction("some-action-name-f", new List<string>() { "304", "102" }); // LeftShift, F
        CustomInputSystem.Instance.RegisterAction("some-action-name-d", new List<string>() { "304", "100" }); // LeftShift, D
        CustomInputSystem.Instance.onActionStartTriggerEvent += onActionStartTriggerEvent;
        CustomInputSystem.Instance.onActionEndTriggerEvent   += onActionEndTriggerEvent;

        // Remove Shortcuts
        CustomInputSystem.Instance.UnregisterAction("some-action-name-d");

        // Mouse Movement Event
        CustomInputSystem.Instance.onMouseMoveEvent += onMouseMoveEvent;

        // Mouse Scroll Event
        CustomInputSystem.Instance.onMouseScrollEvent += onMouseScrollEvent;

        // Any Key Events (not recoomended to use, or only for short times, like checking keys user pressed, to bind in settings)
        // Note that you will NOT receive KeyUp events if this option is disabled, while user is still holding any key.
        CustomInputSystem.Instance.enableAllKeyEvents = true; // Disabled by default
        CustomInputSystem.Instance.onAnyKeyDown += onAnyKeyDown;
        CustomInputSystem.Instance.onAnyKeyUp   += onAnyKeyUp;
    }

    private void onAnyKeyDown( string name, KeyCode keyCode ) {
        Debug.Log($"onAnyKeyDown triggered - name: '{name}'; keyCode: '{keyCode}';");
    }

    private void onAnyKeyUp( string name, KeyCode keyCode ) {
        Debug.Log($"onAnyKeyUp triggered - name: '{name}'; keyCode: '{keyCode}';");
    }

    private void onMouseMoveEvent( Vector2 movedPos, Vector2 globalPos ) {
        Debug.Log($"onMouseMoveEvent triggered - movedPos: '{movedPos}'; globalPos: '{globalPos}';");
    }

    private void onMouseScrollEvent( Vector2 scrolledAmount ) {
        Debug.Log($"onMouseScrollEvent triggered - scrolledAmount: '{scrolledAmount}';");
    }

    private void onActionStartTriggerEvent( string action ) {
        Debug.Log( $"onActionStartTriggerEvent triggered - action: '{action}';" );
    }

    private void onActionEndTriggerEvent( string action ) {
        Debug.Log( $"onActionEndTriggerEvent triggered - action: '{action}';" );
    }

}

```

<br/>

## License
This project is licensed under
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) license,
which means this plugin is free for personal use only.

Contact info:
<br/>
[https://github.com/EdCordata](https://github.com/EdCordata)
