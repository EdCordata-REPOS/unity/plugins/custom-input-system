using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/*

    REQUIREMENTS
    -----------------------------------------------------------------------------------------
    Create a GameObject (for example called "Global") and attach this script
    -----------------------------------------------------------------------------------------


    Description
    -----------------------------------------------------------------------------------------
    Custom input system, by EdCordata
    https://gitlab.com/EdCordata-REPOS/unity/plugins/custom-input-system
    -----------------------------------------------------------------------------------------


    Usage:
    -----------------------------------------------------------------------------------------

    void Start() {

        // Add Shortcut
        CustomInputSystem.Instance.RegisterAction("some-action-name-f", new List<string>() { "304", "102" }); // LeftShift, F
        CustomInputSystem.Instance.RegisterAction("some-action-name-d", new List<string>() { "304", "100" }); // LeftShift, D
        CustomInputSystem.Instance.onActionStartTriggerEvent += onActionStartTriggerEvent;
        CustomInputSystem.Instance.onActionEndTriggerEvent   += onActionEndTriggerEvent;

        // Remove Shortcuts
        CustomInputSystem.Instance.UnregisterAction("some-action-name-d");

        // Mouse Movement Event
        CustomInputSystem.Instance.onMouseMoveEvent += onMouseMoveEvent;

        // Mouse Scroll Event
        CustomInputSystem.Instance.onMouseScrollEvent += onMouseScrollEvent;

        // Any Key Events (not recoomended to use, or only for short times, like checking keys user pressed, to bind in settings)
        // Note that you will NOT receive KeyUp events if this option is disabled, while user is still holding any key.
        CustomInputSystem.Instance.enableAllKeyEvents = true; // Disabled by default
        CustomInputSystem.Instance.onAnyKeyDown += onAnyKeyDown;
        CustomInputSystem.Instance.onAnyKeyUp   += onAnyKeyUp;
    }

    private void onAnyKeyDown( string name, KeyCode keyCode ) {
        Debug.Log($"onAnyKeyDown triggered - name: '{name}'; keyCode: '{keyCode}';");
    }

    private void onAnyKeyUp( string name, KeyCode keyCode ) {
        Debug.Log($"onAnyKeyUp triggered - name: '{name}'; keyCode: '{keyCode}';");
    }

    private void onMouseMoveEvent( Vector2 movedPos, Vector2 globalPos ) {
        Debug.Log($"onMouseMoveEvent triggered - movedPos: '{movedPos}'; globalPos: '{globalPos}';");
    }

    private void onMouseScrollEvent( Vector2 scrolledAmount ) {
        Debug.Log($"onMouseScrollEvent triggered - scrolledAmount: '{scrolledAmount}';");
    }

    private void onActionStartTriggerEvent( string action ) {
        Debug.Log( $"onActionStartTriggerEvent triggered - action: '{action}';" );
    }

    private void onActionEndTriggerEvent( string action ) {
        Debug.Log( $"onActionEndTriggerEvent triggered - action: '{action}';" );
    }

    -----------------------------------------------------------------------------------------

 */
public class CustomInputSystem : MonoBehaviour {


    // Keep alive as global instance
    // ======================================================================
    public static CustomInputSystem Instance { get; private set; }
    private void Awake() { Instance = this; }
    // ======================================================================


    // Public Events
    // ======================================================================

    public event Action<Vector2,Vector2> onMouseMoveEvent;
    public void ToggleMouseMoveEvent( Vector2 movedPos, Vector2 globalPos ) {
        if ( onMouseMoveEvent != null ) onMouseMoveEvent(movedPos, globalPos);
    }

    public event Action<Vector2> onMouseScrollEvent;
    public void ToggleMouseScrollEvent( Vector2 scrolledAmount ) {
        if ( onMouseScrollEvent != null ) onMouseScrollEvent(scrolledAmount);
    }

    public event Action<string> onActionStartTriggerEvent;
    public void ToggleActionStartTriggerEvent( string action ) {
        if ( onActionStartTriggerEvent != null ) onActionStartTriggerEvent(action);
    }

    public event Action<string> onActionEndTriggerEvent;
    public void ToggleActionEndTriggerEvent( string action ) {
        if ( onActionEndTriggerEvent != null ) onActionEndTriggerEvent(action);
    }

    public event Action<string, KeyCode> onAnyKeyDown;
    public void ToggleAnyKeyDown( string name, KeyCode keyCode ) {
        if ( onAnyKeyDown != null ) onAnyKeyDown(name, keyCode);
    }

    public event Action<string, KeyCode> onAnyKeyUp;
    public void ToggleAnyKeyUp( string name, KeyCode keyCode ) {
        if ( onAnyKeyUp != null ) onAnyKeyUp(name, keyCode);
    }

    // ======================================================================


    // Public Methods
    // ======================================================================

    public List<string> GetAllKeysAvailable() {
        if ( allKeysAvailable.Count == 0 ) generateAllKeysAvailable();
        return allKeysAvailable.Keys.ToList();
    } // GetAllKeysAvailable


    public void RegisterAction( string name, List<string> keys ) {
        if ( keys.Count == 0 ) {
            Debug.LogError($"CustomInputSystem: cannot register an action without any keys");
            return;
        }

        if ( registeredKeybindings.ContainsKey(name) ) {
            Debug.LogError($"CustomInputSystem: action with a name '{name}' has already been registered.");
            return;
        }

        registeredKeybindings.Add(name, keys);
    } // RegisterAction


    public void UnregisterAction( string name) {
        if ( registeredKeybindings.ContainsKey(name) ) {
            registeredKeybindings.Remove(name);
        } else {
            Debug.LogError($"CustomInputSystem: action with a name '{name}' is not found.");
        }
    } // RegisterAction

    // ======================================================================


    // Public Variables
    // ======================================================================
    public bool enableAllKeyEvents = false;
    public readonly string version = "1.3";
    // ======================================================================


    // Private Variables
    // ======================================================================
    private Nullable<Vector2>                mouseLastPos          = null;
    private List<string>                     actionsTriggered      = new List<string>();
    private Dictionary<string, List<string>> registeredKeybindings = new Dictionary<string, List<string>>();
    private Dictionary<string, KeyCode>      allKeysAvailable      = new Dictionary<string, KeyCode>();
    private List<string>                     allKeyEventsPressed   = new List<string>();
    // ======================================================================


    // Private Events
    // ======================================================================

    private void Start() {
        generateAllKeysAvailable();
    }

    private void Update() {
        TickAnyKeyDownEvent();
        TickAnyKeyUpEvent();
        TickMouseMoveListener();
        TickMouseScrollListener();
        TickKeyboardEventsListener();
    }

    // ======================================================================


    // Private Methods
    // ======================================================================

    private void TickAnyKeyDownEvent() {
        if ( enableAllKeyEvents ) {
            if ( Input.anyKeyDown ) {
                foreach ( var keyCode in allKeysAvailable.Values ) {
                    if ( Input.GetKey(keyCode) ) {
                        string keyDecimalStr = ((decimal)keyCode).ToString();

                        if ( !allKeyEventsPressed.Contains(keyDecimalStr) ) {
                            allKeyEventsPressed.Add(keyDecimalStr);
                            ToggleAnyKeyDown(keyDecimalStr, keyCode);
                        }
                    }
                }
            }
        }
    } // TickAnyKeyDownEvent


    private void TickAnyKeyUpEvent() {
        if ( enableAllKeyEvents ) {
            List<string> allKeyEventsPressedToRemove = new List<string>();

            foreach ( var keyDecimalStr in allKeyEventsPressed ) {
                var keyCode = allKeysAvailable[keyDecimalStr];

                if ( !Input.GetKey(keyCode) ) {
                    ToggleAnyKeyUp(keyDecimalStr, keyCode);
                    allKeyEventsPressedToRemove.Add(keyDecimalStr);
                }
            }

            foreach ( var keyDecimalStr in allKeyEventsPressedToRemove ) {
                allKeyEventsPressed.Remove(keyDecimalStr);
            }
        } else {
            // TODO: see if this can be improved, as these two if's will run every update
            if ( allKeyEventsPressed.Count > 0 ) allKeyEventsPressed.Clear();
        }
    } // TickAnyKeyUpEvent


    private void generateAllKeysAvailable() {
        foreach ( KeyCode keyCode in System.Enum.GetValues(typeof(KeyCode)) ) {
            string keyDecimalStr = ((decimal)keyCode).ToString();

            // GetValues sometimes contain the same keyCode name twice.
            if ( !allKeysAvailable.ContainsKey(keyDecimalStr) ) {
                allKeysAvailable.Add(keyDecimalStr, keyCode);
            }
        }
    } // generateAllKeysAvailable


    private void TickMouseMoveListener() {
        Vector2 mousePos = new Vector2(
            Input.mousePosition.x,
            Input.mousePosition.y
        );

        if ( mouseLastPos == null ) {
            mouseLastPos = mousePos;
        } else {
            if ( mouseLastPos != mousePos ) {
                Vector2 movedPos = mousePos - (Vector2)mouseLastPos; // Cast to Vector2, cos mouseLastPos is Nullable<Vector2>
                ToggleMouseMoveEvent(movedPos, mousePos);
                mouseLastPos = mousePos;
            }
        }
    } // TickMouseEventsListener


    private void TickMouseScrollListener() {
        if ( Input.mouseScrollDelta != Vector2.zero ) ToggleMouseScrollEvent(Input.mouseScrollDelta);
    } // TickMouseScrollListener


    private void TickKeyboardEventsListener() {
        // loop through all registered actions
        foreach ( var pair in registeredKeybindings ) {
            string       action           = pair.Key;
            List<string> keys             = pair.Value;
            int          keysPressedCount = 0;

            // update keysPressedCount (used to detect if action should be triggered)
            foreach ( var keyDecimal in keys ) {
                KeyCode key = allKeysAvailable[keyDecimal];
                if ( Input.GetKey(key) ) keysPressedCount++;
            }

            // check keysPressedCount to be same as pressed key count in current action to detect if action
            // should be triggered (and trigger, unless already triggered)
            if ( keysPressedCount == keys.Count ) {
                // if action is in actionsTriggered list, that means, that it has already been triggered, and should
                // not be triggered again. If not, we save & trigger it
                if ( !actionsTriggered.Contains(action) ) {
                    actionsTriggered.Add(action);
                    ToggleActionStartTriggerEvent(action);
                }
            } else {
                // if all keys are not pressed, that at least one of action keys has been released,
                // which means it can be triggered again on next check. So we ensure that it's removed
                // from actionsTriggered
                if ( actionsTriggered.Contains(action) ) {
                    ToggleActionEndTriggerEvent(action);
                    actionsTriggered.Remove(action);
                }
            }
        }
    } // TickKeyboardEventsListener

    // ======================================================================


}
